api = 2
core = 7.x

; Modules
projects[workbench][download][branch] = 7.x-1.x
projects[workbench][download][type] = git
projects[workbench][subdir] = contrib
projects[workbench_access][download][branch] = 7.x-1.x
projects[workbench_access][download][type] = git
projects[workbench_access][subdir] = contrib
projects[workbench_moderation][download][branch] = 7.x-1.x
projects[workbench_moderation][download][type] = git
projects[workbench_moderation][subdir] = contrib
